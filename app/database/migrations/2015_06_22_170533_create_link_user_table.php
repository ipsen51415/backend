<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(App::environment() == 'testing') {
			Schema::create('link_user', function(Blueprint $table)
			{
				$table->integer('link_id');
				$table->integer('user_id');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(App::environment() == 'testing') {
			Schema::table('link_user', function(Blueprint $table)
			{
				Schema::drop('link_user');
			});
		}
	}

}
