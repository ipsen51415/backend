<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluatieLinkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('evaluatie_link', function(Blueprint $table)
		{
			if(App::environment() == 'testing') {
				$table->increments('id');
				$table->integer('evaluatie_creator_id');
				$table->integer('week');
				$table->integer('year');
				$table->timestamps();			
			}
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('evaluatie_link', function(Blueprint $table)
		{
			if(App::environment() == 'testing') {
				Schema::drop('evaluatie_link');
			}
		});
	}

}
