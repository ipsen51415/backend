<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(App::environment() == 'testing') {
			Schema::create('user', function(Blueprint $table)
			{
				$table->increments('id');
				$table->string('unikey', 45);
				$table->string('email', 200);
				$table->string('password');
				$table->integer('points');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(App::environment() == 'testing') {
			Schema::table('user', function(Blueprint $table)
			{
				Schema::drop('user');
			});
		}
	}

}
