<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluatieAnswerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(App::environment() == 'testing') {
			Schema::create('evaluatie_answer', function(Blueprint $table)
			{
				$table->increments('id');
				$table->integer('user_id');
				$table->integer('link_id');
				$table->integer('question_id');
				$table->integer('answer');
				$table->integer('responsetime');
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(App::environment() == 'testing') {
			Schema::table('evaluatie_answer', function(Blueprint $table)
			{
				Schema::drop('evaluatie_answer');
			});
		}
	}

}
