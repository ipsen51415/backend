<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluatieQuestionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('evaluatie_question', function(Blueprint $table)
		{
			if(App::environment() == 'testing') {
				$table->increments('id');
				$table->integer('evaluatie_creator_id');
				$table->enum('type', array('janee', 'ster'));
				$table->string('question', 200);
				$table->timestamps();			
			}
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('evaluatie_question', function(Blueprint $table)
		{
			if(App::environment() == 'testing') {
				Schema::drop('evaluatie_question');
			}
		});
	}

}
