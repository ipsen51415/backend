<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluatieCreatorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(App::environment() == 'testing') {
			Schema::create('evaluatie_creator', function(Blueprint $table)
			{
				$table->increments('id');
				$table->integer('evaluatie_hogeschool_id');
				$table->string('name', 45);
				$table->string('email');
				$table->string('password');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('evaluatie_creator', function(Blueprint $table)
		{
			if(App::environment() == 'testing') {
				Schema::drop('evaluatie_creator');
			}
		});
	}

}
