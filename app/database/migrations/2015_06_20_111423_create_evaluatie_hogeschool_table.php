<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluatieHogeschoolTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(App::environment() == 'testing') {
			Schema::create('evaluatie_hogeschool', function(Blueprint $table)
			{
				$table->increments('id');
				$table->string('hogeschool');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(App::environment() == 'testing') {
			Schema::table('evaluatie_hogeschool', function(Blueprint $table)
			{
				Schema::drop('evaluatie_hogeschool');
			});
		}
	}

}
