<?php

class QuestionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$questions = Question::all();
        return View::make('question.overview')->with('questions', $questions);
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$lastQuestion = Question::orderBy('id', 'desc')->first();
		if ($lastQuestion) {
			$lastID = $lastQuestion->id + 1;
		} else {
			$lastID = 1;
		}

		$question = new Question();
		$question->id = $lastID;
		$question->fill(Input::get('Question'));
		$question->evaluatie_creator_id = Auth::user()->id;

		if ($question->save()) {
	    	$question->save();
			return View::make('question.view')->with('question', $question);
        }
	    else{
			return Redirect::back()->with('errors', $question->errors());
        }

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$question = Question::where('id', $id)->firstOrFail();
		return View::make('question.view')->with('question', $question);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$question = Question::findOrFail($id);
		$question->fill(Input::get('Question'));

		if ($question->save()) {
	    	$question->save();
			return View::make('question.view')->with('question', $question);
        }
	    else{
			return Redirect::to('question/' . $id)->with('errors', $question->errors());
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$question = Question::findOrFail($id);

		$answers = \Answer::where('question_id', $question->id)->get();
		foreach ($answers as $answer) {
			$answer->delete();
		}
		$links = $question->link;
		foreach ($links as $link) {
			$question->link()->detach($link->id);
		}
		$question->delete();

        return Redirect::action('QuestionController@index');
	}
	/**
	 * Attach a link too a question
	 *
	 * @param  link $link
	 * @param  int  $id
	 * @return Response
	 */
	public function attach($question_id, $link_id){
		$question = Question::findOrFail($question_id);
		$question->link()->attach($link_id);

		return Redirect::action('QuestionController@show', array('id' => $question_id));
	}

	/**
	 * Remove a question from a link
	 *
	 * @param  link $link
	 * @param  int  $id
	 * @return Response
	 */
	public function detach($question_id, $link_id){
		$question = Question::findOrFail($question_id);

		$answers = \Answer::where('link_id', $link_id)->where('question_id', $question_id)->get();
		foreach ($answers as $answer) {
			$answer->delete();
		}
		
		$question->link()->detach($link_id);
		return Redirect::action('QuestionController@show', array('id' => $question_id));
	}
}
