<?php

class APIController extends \BaseController {

	/**
	 * Create a JSON with all the questions for a specific link
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getQuestions($id)
	{
		$link = Link::where('id', $id)->firstOrFail();

		return Response::json(array('status' => 'success', 'data' => $link->question));
	}

	/**
	 * Create a User and generate a JSON with the newly created User
	 *
	 * @return Response
	 */
	public function storeUser()
	{
		$lastUser = User::orderBy('id', 'desc')->first();
		if ($lastUser) {
			$lastID = $lastUser->id + 1;
		} else {
			$lastID = 1;
		}

		$user = new User();
		$user->id = $lastID;
		$user->unikey = Input::get('unikey');
		$user->email = Input::get('email');
		$user->password = Input::get('password');

		if ($user->save()){
			$user->save();
			return Response::json(array('status'=> 'success', 'data' => $user));
		}
		else{
			return Response::json(array('status'=> 'failed', 'message' => $user->errors()), 400);
		}		
	}	

	/**
	 * Create a JSON with a specific user
	 *
	 * @param  int  $email
	 * @return Response
	 */
	public function getUser($email, $password)
	{
		$users = User::all();
		foreach ($users as $user) {
			if ($user->email == $email && $user->password == $password){
				return Response::json(array('status' => 'success', 'data' => $user));
			}
		}
		return Response::json(array('status' => 'failure', 'message' => 'Incorrect email and password combination'), 404);
	}

	/**
	 * Create a JSON with confirmation wether email exists
	 *
	 * @param  int  $email
	 * @return Response
	 */
	public function getEmail($email)
	{
		if (User::where('email', '=', $email)->exists()){
			return Response::json(array('status' => 'failure', 'message' => 'An account with this email already exists'), 400);
		}
		else{
			return Response::json(array('status' => 'success', 'message' => 'Email is still available'));
		}
	}

	/**
	 * Create a JSON with currently open links
	 *
	 * @return Response
	 */
	public function getLinks($user_id)
	{
		$currentDate = Carbon\Carbon::now();
		$week = $currentDate->format('W');
		$year = $currentDate->format('o');

		$currentUser = User::findOrFail($user_id);
		$openLinks = Link::has('question')->where('week', $week)->where('year', $year)->get();
		$userLinks = array();

		foreach ($openLinks as $openLink) {
			if (!$currentUser->link->find($openLink)){
				array_push($userLinks, $openLink);
			}
		}

		if (count($userLinks) > 0){
			return Response::json(array('status' => 'success', 'data' => $userLinks));
		}
		else{
			return Response::json(array('status'=> 'success', 'message' => 'There are no available evaluations for this week'), 404);
		}
	}

	/**
	 * Store a newly create linkuser in storage
	 *
	 * @return Response
	 */
	public function storeLinkUser()
	{
		try{
			$link = Link::findOrFail(Input::get('link_id'));
			$user = User::findOrFail(Input::get('user_id'));
			$link->user()->attach($user->id);
			return Response::json(array('status' => 'success', 'message' => 'LinkUser succesfully stored in database'));
		}
		catch (Exception $ex){
			return Response::json(array('status' => 'failed', 'message' => $ex), 400);
		}

	}

	/**
	 * Store a newly created answer in storage.
	 *
	 * @return Response
	 */
	public function storeAnswer()
	{
		$lastAnswer = Answer::orderBy('id', 'desc')->first();
		if ($lastAnswer) {
			$lastID = $lastAnswer->id + 1;
		} else {
			$lastID = 1;
		}

		$answer = new Answer();
		$answer->id = $lastID;
		$answer->user_id = Input::get('user_id');
		$answer->link_id = Input::get('link_id');
		$answer->question_id = Input::get('question_id');
		$answer->answer = Input::get('answer');
		$answer->responsetime = Input::get('responsetime');

		if ($answer->save()){
			$answer->save();
			if ($answer->responsetime >= 1000) {
				$user = User::findOrFail($answer->user_id);
				$user->points = $user->points + 1;
				if ($user->save()){
					$user->save();
				}
				else {
					var_dump($user->errors());
				}
			}
			return Response::json(array('status'=> 'success', 'message' => 'Answer succesfully stores in database'));
		}
		else{
			return Response::json(array('status'=> 'failed', 'message' => $answer->errors()), 400);
		}
	}
}
