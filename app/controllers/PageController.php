<?php

class PageController extends \BaseController {

	/**
	 * Display the homepage of the application.
	 *
	 * @return Response
	 */
	public function home()
	{
		return View::make('pages.home')->with('creator', Auth::user());
	}
}
