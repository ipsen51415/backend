<?php

class LinkController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$links = Link::all();
		return View::make('link.overview')->with('links', $links);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$lastLink = Link::orderBy('id', 'desc')->first();
		if ($lastLink) {
			$lastID = $lastLink->id + 1;
		} else {
			$lastID = 1;
		}

		$link = new Link();
		$link->id = $lastID;
		$link->fill(Input::get('Link'));
		$link->evaluatie_creator_id = Auth::user()->id;

        if ($link->save()) {
	    	$link->save();
			$ids = \DB::table('link_question')->where('link_id', '=', $lastID)->lists('question_id');
			$answers = \Answer::where('id', $lastID)->whereIn('question_id', $ids)->get();

			return View::make('link.view')->with('link', $link)->with('answers', $answers);
        }
	    else{
			return Redirect::back()->with('errors', $link->errors());
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$link = Link::where('id', $id)->firstOrFail();

		$ids = \DB::table('link_question')->where('link_id', '=', $id)->lists('question_id');
		$answers = \Answer::where('link_id', $id)->whereIn('question_id', $ids)->get();

		foreach ($answers as $answer) {
			$question = Question::findOrFail($answer->question_id);
			if ($question->type == 'ster' and $answer->answer == 0){
				$key = $answers->search($answer);
				$answers->forget($key);
			}
		}

		return View::make('link.view')->with('link', $link)->with('answers', $answers);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Attach a question too a link
	 *
	 * @param  link $link
	 * @param  int  $id
	 * @return Response
	 */
	public function attach($link_id, $question_id){
		$link = Link::findOrFail($link_id);

		$link->question()->attach($question_id);

		$ids = \DB::table('link_question')->where('link_id', '=', $link_id)->lists('question_id');

		return Redirect::action('LinkController@show', array('id' => $link_id));
	}

	/**
	 * Remove a question from a link
	 *
	 * @param  link $link
	 * @param  int  $id
	 * @return Response
	 */
	public function detach($link_id, $question_id){
		$link = Link::findOrFail($link_id);

		$answers = \Answer::where('link_id', $link_id)->where('question_id', $question_id)->get();

		foreach ($answers as $answer) {
			$answer->delete();
		}
		$link->question()->detach($question_id);

		return Redirect::action('LinkController@show', array('id' => $link_id));
	}
}
