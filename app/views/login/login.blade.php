<!doctype html>
<html lang="en">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<!-- FONTAWESOME STYLES-->
    <link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet" />
	<!-- CUSTOM STYLES-->
	<link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet" />
	<!-- GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div class="container">
        <div class="row text-center ">
            <div class="col-md-12">
                <br /><br />
                <h2>Stucomm</h2>
               
                <h5>Inloggen op het evaluatie beheersysteem.</h5>
                 <br />
            </div>
        </div>
         <div class="row ">
                  <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                      @if(isset($msg))
                      <div class="alert alert-danger" role="alert">{{$msg}}</div>
                      @endif
                        <div class="panel panel-default">
                            <div class="panel-heading">
                        <strong>Inloggen</strong>  
                            </div>
                            <div class="panel-body">
                                <form role="form" action="{{URL::action('LoginController@login')}}" method="post">
                                       <br />
                                     <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                            <input type="email" name="email" class="form-control" placeholder="E-mail" required/>
                                        </div>
                                                                              <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                                            <input type="password" name="password" class="form-control"  placeholder="Wachtwoord" required/>
                                        </div>
                                     
                                     <button type="submit" class="btn btn-primary ">Login</button>
                                    </form>
                            </div>
                           
                        </div>
                    </div>
                
                
        </div>
    </div>   
</body>
</html>
