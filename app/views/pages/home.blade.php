<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/custom.css">
    <!-- FONTAWESOME STYLES-->
    <link href="css/font-awesome.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ URL::action('PageController@home') }}">Stucomm</a> 
            </div>
            <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
                <a class="text-white" href="{{URL::to('/logout')}}" >Log uit</a>
                |
                <a class="text-white" href="#" >?</a>
            </div>
        </nav>   
        <!-- /. NAV TOP  -->

        <div id="page-wrapper-home" >
            <div id="page-inner">
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif  
                <div class="row">
                    <div class="col-md-3 col-md-offset-1">
                        <a href="{{ URL::action('LinkController@index') }}">
                            <button type="button" class="btn btn-black btn-circle-medium">Evaluaties <br>inzien</button>
                        </a>
                    </div>
                    <div class="col-md-3 col-md-offset-3">
                        <a href="{{ URL::action('QuestionController@index') }}">
                            <button type="button" class="btn btn-red btn-circle-medium">Vragen <br>inzien</button>
                        </a>
                    </div>
                </div>
                <!-- /. ROW  --> 

                <div class="row">
                    <div class="col-md-3-bottom col-md-offset-1">
                            <button type="button" class="btn btn-red btn-circle-medium" data-toggle="modal" data-target="#linkCreateModal">Evaluatie <br>aanmaken</button>
                        </a>
                    </div>
                    <div class="col-md-4 col-md-offset-4">
                        <button type="button" class="btn btn-grey btn-circle-large disabled">
                            <img src="img/find_user.png" class="user-image img-responsive" height="256" width="256" />
                            <h2 class="text-dynamic">{{{ $creator->name }}}</h2>
                            <h3 class="text-dynamic">{{{ $creator->email }}}
                                <br>{{{ $creator->hogeschool->hogeschool }}}</h3>
                            </button>
                        </div>
                        <div class="col-md-3 col-md-offset-1">
                            <button type="button" class="btn btn-black btn-circle-medium" data-toggle="modal" data-target="#questionCreateModal">Vraag <br>aanmaken</button>
                        </a>
                    </div>
                </div>
                <!-- /. ROW  --> 
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->

    </div>   
    <!-- /. WRAPPER  -->

    <div class="modal fade" id="questionCreateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-square"></i> Vraag aanmaken</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(array('route' => array('question.store'))) }}
                    <label>Type:</label><br>
                    {{ Form::radio('Question[type]', 'janee', true) }}Ja / Nee<br>
                    {{ Form::radio('Question[type]', 'ster') }}Ster<br><br>
                    <label>Vraag:</label><br>
                    {{ Form::text('Question[question]', null, array('class' => 'form-control')) }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                    {{ Form::submit('Opslaan', ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="linkCreateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-square"></i> Evaluatiemoment toevoegen</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(array('route' => array('link.store'))) }}
                    <label>Jaar:</label><br>
                    {{ Form::text('Link[year]', null, array('class' => 'form-control')) }}
                    <label>Week:</label><br>
                    {{ Form::text('Link[week]', null, array('class' => 'form-control', 'placeholder' => '1 tot 52')) }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                    {{ Form::submit('Opslaan', ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->

</body>
</html>
