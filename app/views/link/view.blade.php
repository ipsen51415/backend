<!doctype html>
<html lang="en">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="{{URL::asset('/js/chart.min.js')}}"></script>
    <!-- FONTAWESOME STYLES-->
    <link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- DataTables -->
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css">
    <script type="text/javascript" charset="utf-8" src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ URL::action('PageController@home') }}">Stucomm</a> 
            </div>
            <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
                <a class="text-white" href="{{URL::to('/logout')}}" >Log uit</a>
                |
                <a class="text-white" href="#" >?</a>
            </div>
        </nav>   
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li class="text-center">
                        <img src="{{ URL::asset('img/find_user.png') }}" class="user-image img-responsive"/>
                        <h3 class="text-white">{{{ Auth::user()->name }}}</h3>
                    </li>
                    <li>
                        <a class="active-menu" href="{{ URL::action('LinkController@index') }}"><i class="fa fa-bar-chart-o fa-3x"></i> Evaluatieoverzicht</a>
                    </li>
                    <li>
                        <a data-toggle="modal" data-target="#linkCreateModal"><i class="fa fa-plus-circle fa-3x"></i>Evaluatie aanmaken</a>
                    </li>
                    <li>
                        <a  href="{{ URL::action('QuestionController@index') }}"><i class="fa fa-question-circle fa-3x"></i> Vragenoverzicht</a>
                    </li>
                    <li  >
                        <a data-toggle="modal" data-target="#questionCreateModal"><i class="fa fa-plus-circle fa-3x"></i>Vraag aanmaken</a>
                    </li>   
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
            @if ($errors->any())
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif  
                <div class="row">
                    <div class="col-md-12">
                        <h2>Evaluatie #{{{ $link->id}}}</h2>
                    </div>
                </div>
                <!-- /. ROW  -->
                <hr />
                <b>Aangemaakt door:</b> {{{ $link->creator->name }}}<br>
                <b>Week:</b> {{{ $link->week }}}<br>
                <b>Jaar:</b> {{{ $link->year }}}<br>
                <b>Geëvalueerd door:</b> {{{ count($link->user) }}} personen<br>
                <hr />
                @if (count($answers) == 0)
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Gekoppelde vragen
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Vraag ID</th>
                                                <th>Aangemaakt door</th>
                                                <th>Vraag</th>
                                                <th>Type</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($link->question as $question)
                                            <tr>
                                                <td>{{{ $question->id }}}</td>
                                                <td>{{{ $question->creator->name }}}</td>
                                                <td>{{{ $question->question }}}</td>
                                                <td>{{{ $question->type }}}</td>
                                                <td>
                                                    {{ Form::open(array('url' => URL::action('LinkController@detach', array($link->id, $question->id)), 'method' => 'delete', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
                                                    {{ Form::submit('Ontkoppelen', ['class' => 'btn btn-primary']) }}
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Beschikbare vragen
                            </div>
                            <div class="panel-body">
                                <div class="table">
                                    <table id="dt" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Vraag ID</th>
                                                <th>Aangemaakt door</th>
                                                <th>Vraag</th>
                                                <th>Type</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($link->availableQuestions() as $question)
                                            <tr>
                                                <td>{{{ $question->id }}}</td>
                                                <td>{{{ $question->creator->name }}}</td>
                                                <td>{{{ $question->question }}}</td>
                                                <td>{{{ $question->type }}}</td>
                                                <td>
                                                    {{ Form::open(array('url' => URL::action('LinkController@attach', array($link->id, $question->id)), 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
                                                    {{ Form::submit('Koppelen', ['class' => 'btn btn-primary']) }}
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Aantal antwoorden per ster vraag
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive text-center" id="barchart"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Aantal antwoorden per ja/nee vraag
                            </div>
                            <div class="panel-body" id="piecharts">
                                <div class="table-responsive text-center col-md-3" id="pie1"></div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

        <div class="modal fade" id="questionCreateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-square"></i> Vraag aanmaken</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(array('route' => array('question.store'))) }}
                    <label>Type:</label><br>
                    {{ Form::radio('Question[type]', 'janee', true) }}Ja / Nee<br>
                    {{ Form::radio('Question[type]', 'ster') }}Ster<br><br>
                    <label>Vraag:</label><br>
                    {{ Form::text('Question[question]', null, array('class' => 'form-control')) }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                    {{ Form::submit('Opslaan', ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="linkCreateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-square"></i> Evaluatiemoment toevoegen</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(array('route' => array('link.store'))) }}
                    <label>Jaar:</label><br>
                    {{ Form::text('Link[year]', null, array('class' => 'form-control')) }}
                    <label>Week:</label><br>
                    {{ Form::text('Link[week]', null, array('class' => 'form-control', 'placeholder' => '1 tot 52')) }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                    {{ Form::submit('Opslaan', ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <?php
        $array = array();
        foreach ($link->question as $question) {
            foreach ($answers as $answer) {
                if($answer->question_id == $question->id){
                    $q = array('qid' => $question->id, 'question' => $question->question, 'answer' => $answer->answer, 'type' => $question->type);
                    array_push($array, $q);
                }
            }
        }
        echo "<script>var chartdata = " . json_encode($array) . ';</script>';
    ?>
    <script>
        var questions = {
            //all is the same as type 'ster' questions
            star: {
                //a lower level creation of the bar charts
                create: function (barData) {
                    var div = document.getElementById('barchart');
                    div.innerHTML += '<canvas id=\"barchr\" ></canvas>';
                    
                    var pchr = document.getElementById('barchr').getContext('2d');
                    new Chart(pchr).Bar(barData, {
                        responsive: true,
                        scaleOverride : true,
                        scaleSteps : 5,
                        scaleStepWidth : 1,
                        scaleStartValue : 0 
                    });
                },
                //returns an array to be used bij ChartJS to fill the bar chart
                getBarData: function (labels, labelData) {
                    var dataBar = {
                        labels: labels,
                        datasets: [
                            {
                                label: "Alle antwoorden",
                                fillColor: "rgba(255, 44, 44, 0.9)",
                                strokeColor: "rgba(151,187,205,0.8)",
                                highlightFill: "rgba(178, 44, 44, 0.9)",
                                highlightStroke: "rgba(151,187,205,1)",
                                data: labelData
                            }
                        ]
                    };
                    
                    return dataBar;
                },
                //will strip the original chartdata array of all questions other then type 'ster', leaves the chartdata array intact
                //requires a collection of raw chart info in this format:
                //[{'qid': 1, 'question': 'A simple question', 'answer': 'A simple answer', 'type': 'ster'}, ...]
                allChartData: function (collection) {
                    var splicedchartdata = [];
                    for(var i = 0; i < collection.length; i++) {
                        if(collection[i].type === 'ster') {
                            splicedchartdata.push(collection[i]);
                        }
                    }
                    
                    return splicedchartdata;
                },
                //gets all the labels for the bar chart, requires a filtered on type collection of questions; use function allChartData()
                getLabels: function (allChartData) {
                    var labels = [];

                    for(var i = 0; i < allChartData.length; i++) {
                        if(!this.contains(allChartData[i].question, labels)) {
                            labels.push(allChartData[i].question);
                        }
                    }
                    
                    return labels;
                },
                //gets all the data associated with the labels for the bar chart
                //returns the average ster answer per question in an array
                labelData: function (labels, allChartData) {
                    var lbldata = [];
                    var tmpdata = [];
                    
                    for(var i = 0; i < labels.length; i++) {
                        lbldata.push({
                            question: labels[i],
                            answers: 0,
                            totalscore: 0,
                            avgscore: 0
                        });
                    }
                    
                    for(var j = 0; j < allChartData.length; j++) {
                        for(var k = 0; k < lbldata.length; k++) {
                            if(allChartData[j].question == lbldata[k].question) {
                                lbldata[k].answers++;
                                lbldata[k].totalscore += allChartData[j].answer;
                            }
                        }
                    }
                    
                    //final for loop, calculate the average score
                    //and redefine lbldata array, we take out all the object and return
                    //an array with only the averagescore
                    for(var l = 0; l < lbldata.length; l++) {
                        lbldata[l].avgscore = lbldata[l].totalscore / lbldata[l].answers;
                        tmpdata.push(lbldata[l].avgscore);
                    }
                    
                    return tmpdata;
                },
                //checks if obj is found in collection
                contains: function(obj, collection) {
                    for(var i = 0; i < collection.length; i++) {
                        if(collection[i] === obj) {
                            return true;
                        }
                    }
                    return false;
                },
                //initializes the bar chart
                setupCharts: function (collection) {
                    var barRawData = this.allChartData(collection);
                    var labels = this.getLabels(barRawData);
                    this.create(
                        this.getBarData(
                        labels, this.labelData(labels, barRawData)));
                }
            },
            //yesno is the same as type 'ster' questions
            yesno: {
                //used to append to html canvassen to make the identifier unique
                pieId: 0,
                create: function (data, question) {
                    this.pieId++;
                    var div = document.getElementById('pie' + this.pieId);
                    div.innerHTML += '<p>' + question + '</p><canvas id=\"piec' + this.pieId + '\" width=100 height=100></canvas>';
                    
                    var pchr = document.getElementById('piec' + this.pieId).getContext('2d');
                    new Chart(pchr).Pie(data);
                },
                //returns an array to be used bij ChartJS to fill the piechart
                getPieData: function (yesCount, noCount) {
                    var pieData = [{
                        value: yesCount,
                        label: 'Ja',
                        color: '#22E667'
                    }, {
                        value: noCount,
                        label: 'Nee',
                        color: '#D61313'
                    }];
                    
                    return pieData;
                },
                //gets the total yes answers for a specific question
                getYesCount: function (data, question) {
                    var count = 0;
                    
                    for(var i = 0; i < data.length; i++) {
                        if(data[i].question === question && data[i].answer === 1) {
                            count++;
                        }
                    }
                    
                    return count;
                },
                //gets the total no answers for a specific question
                getNoCount: function (data, question) {
                    var count = 0;
                    
                    for(var i = 0; i < data.length; i++) {
                        if(data[i].question === question && data[i].answer === 0) {
                            count++;
                        }
                    }
                    
                    return count;
                },
                //will strip the original chartdata array of all questions other then type 'janee', leaves the chartdata array intact
                //requires a collection of raw chart info in this format:
                //[{'qid': 1, 'question': 'A simple question', 'answer': 'A simple answer', 'type': 'ster'}, ...]
                yesnoChartData: function (collection) {
                    var splicedchartdata = [];
                    for(var i = 0; i < collection.length; i++) {
                        if(collection[i].type === 'janee') {
                            splicedchartdata.push(collection[i]);
                        }
                    }
                    
                    return splicedchartdata;
                },
                //checks if a obj is in the collection
                contains: function (obj, collection) {
                    for(var i = 0; i < collection.length; i++) {
                        if(collection[i] === obj) {
                            return true;
                        }
                    }
                    return false;
                },
                //will count the amount of unique janee questions
                uniqueQuestions: function (data) {
                    var uq = [];
                    
                    for(var i = 0; i < data.length; i++) {
                        if(!this.contains(data[i].question, uq)) {
                           uq.push(data[i].question);
                        }
                    }
                    
                    return uq;
                },
                fixDivs: function () {
                    //we use pieId attribute to decide which elements to move
                    var parent = document.getElementById('piecharts');
                    for(var i = 0; i < this.pieId; i++) {
                        var element = document.getElementById('pie' + (i + 1));
                        parent.appendChild(element);
                    }
                },
                //call this method to initialize all the pie charts based on the chartdata array
                setupCharts: function (collection) {
                    var pieRawData = this.yesnoChartData(collection);
                    var uquestions = this.uniqueQuestions(pieRawData);
                    
                    for(var i = 0; i < uquestions.length; i++) {
                        //create a new div for the canvas that will be created with the create() function
                        //this is a dirty way to insert the pie charts, but they wont work if we simatenously 
                        //create the div and canvas in the create() method
                        //so to fix the dom afterwards we run fixDivs()
                        var div = document.getElementById('pie' + (i + 1));
                        div.innerHTML += '<div class="table-responsive text-center col-md-3" id="pie' + (i + 2) + '"></div>';
                        
                        this.create(
                            this.getPieData(
                                this.getYesCount(pieRawData, 
                                    uquestions[i]), 
                                this.getNoCount(pieRawData, 
                                    uquestions[i])), uquestions[i]);
                    }
                    
                    //fixDivs will rearrange the dom so the piecharts divs are nicely aligned
                    this.fixDivs();
                }
            }
        };
        
        //format of chartdata is: [{'qid': 1, 'question': 'A simple question', 'answer': 'A simple answer', 'type': 'ster'}, ...]
        //wrapped the setupCharts functions in a timeout function to let the browser create the canvassus on time
        //experimental fix for the charts on the production server
        setTimeout(function () {
            questions.yesno.setupCharts(chartdata);
            questions.star.setupCharts(chartdata);
        }, 1000);    
    </script>
    <script>
        $(document).ready(function () {
            $('#dt').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/Dutch.json"
                }
            });
        });
    </script>
</body>
</html>

