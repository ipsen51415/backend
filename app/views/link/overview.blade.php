<!doctype html>
<html lang="en">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<!-- FONTAWESOME STYLES-->
    <link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet" />
	<!-- CUSTOM STYLES-->
	<link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet" />
	<!-- GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- DataTables -->
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css">
    <script type="text/javascript" charset="utf-8" src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    
</head>
<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ URL::action('PageController@home') }}">Stucomm</a> 
			</div>
			<div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
				<a class="text-white" href="{{URL::to('/logout')}}" >Log uit</a>
				|
				<a class="text-white" href="#" >?</a>
			</div>
		</nav>   
		<!-- /. NAV TOP  -->
		<nav class="navbar-default navbar-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav" id="main-menu">
					<li class="text-center">
						<img src="{{ URL::asset('img/find_user.png') }}" class="user-image img-responsive"/>
						<h3 class="text-white">{{{ Auth::user()->name }}}</h3>
					</li>
					<li>
						<a class="active-menu" href="{{ URL::action('LinkController@index') }}"><i class="fa fa-bar-chart-o fa-3x"></i> Evaluatieoverzicht</a>
					</li>
					<li>
						<a data-toggle="modal" data-target="#linkCreateModal"><i class="fa fa-plus-circle fa-3x"></i>Evaluatie aanmaken</a>
					</li>
					<li>
						<a  href="{{ URL::action('QuestionController@index') }}"><i class="fa fa-question-circle fa-3x"></i> Vragenoverzicht</a>
					</li>
					<li  >
                        <a data-toggle="modal" data-target="#questionCreateModal"><i class="fa fa-plus-circle fa-3x"></i>Vraag aanmaken</a>
					</li>	
				</ul>

			</div>

		</nav>  
		<!-- /. NAV SIDE  -->
		<div id="page-wrapper" >
			<div id="page-inner">
			@if ($errors->any())
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif  
				<div class="row">
					<div class="col-md-12">
						<h2>Evaluatie overzicht</h2>
					</div>
				</div>
				<!-- /. ROW  -->
				<hr />
				<div class="panel-body">
					<div class="table">
						<table id="dt" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Evaluatie ID</th>
									<th>Aangemaakt door</th>
									<th>Week</th>
									<th>Jaar</th>
                                    <th>Optie</th>
								</tr>
							</thead>
							<tbody>
							@foreach($links as $link)
								<tr>
									<td>{{{ $link->id }}}</td>
									<td>{{{ $link->creator->name }}}</td>
									<td>{{{ $link->week }}}</td>
									<td>{{{ $link->year }}}</td>
									<td><a href="{{ URL::action('LinkController@show', $link->id) }}" class="btn btn-primary">Details</a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /. PAGE INNER  -->
		</div>
		<!-- /. PAGE WRAPPER  -->
	</div>
	<!-- /. WRAPPER  -->

	    <div class="modal fade" id="questionCreateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-square"></i> Vraag aanmaken</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(array('route' => array('question.store'))) }}
                    <label>Type:</label><br>
                    {{ Form::radio('Question[type]', 'janee', true) }}Ja / Nee<br>
                    {{ Form::radio('Question[type]', 'ster') }}Ster<br><br>
                    <label>Vraag:</label><br>
                    {{ Form::text('Question[question]', null, array('class' => 'form-control')) }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                    {{ Form::submit('Opslaan', ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="linkCreateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-square"></i> Evaluatiemoment toevoegen</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(array('route' => array('link.store'))) }}
                    <label>Jaar:</label><br>
                    {{ Form::text('Link[year]', null, array('class' => 'form-control')) }}
                    <label>Week:</label><br>
                    {{ Form::text('Link[week]', null, array('class' => 'form-control', 'placeholder' => '1 tot 52')) }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                    {{ Form::submit('Opslaan', ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#dt').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/Dutch.json"
                }
            });
        });
    </script>

</body>
</html>

