<?php

class APIControllerTest extends TestCase {

    protected $useDatabase = true;

	public function testGetQuestions()
	{
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $link = $this->createValidLink($creator);
        $link->save();

		$this->call('GET', 'api/link/question/' . $link->id);		
        $this->assertResponseStatus(200);
    }

    public function testStoreUser()
    {
        $input = array(
            'unikey' => 'hsl',
            'email' => 'test@mail.nl',
            'password' => 'Test1234'
        );

        $this->call('POST', 'api/user', $input);
        $this->assertResponseStatus(200);

        $user = User::first();
        $this->assertTrue($user->unikey == 'hsl');
        $this->assertTrue($user->email == 'test@mail.nl');
        $this->assertTrue($user->password == 'Test1234');
    }

    public function testStoreUserFails()
    {
        $input = array(
            'unikey' => 'hsl'
        );

        $this->call('POST', 'api/user', $input);
        $this->assertResponseStatus(400);

        $this->assertTrue(User::all()->isEmpty());
    }

    public function testStoreLinkUser()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $link = $this->createValidLink($creator);
        $link->save();

        $user = $this->createValidUser();
        $user->save();

        $input = array(
            'link_id' => $link->id,
            'user_id' => $user->id
        );

        $this->call('POST', 'api/link/user', $input);
        $this->assertResponseStatus(200);
        $this->assertTrue($link->user->find($user->id)->exists);
    }

    public function testStoreLinkUserFails()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $link = $this->createValidLink($creator);
        $link->save();

        $user = $this->createValidUser();
        $user->save();

        $input = array(
            'link_id' => $link->id
        );

        $this->call('POST', 'api/link/user', $input);
        $this->assertResponseStatus(400);
        $this->assertTrue(empty($link->user));
    }

    public function testGetUser()
    {
        $user = $this->createValidUser();
        $user->save();

        $this->call('GET', 'api/user/' . $user->email . '/' . $user->password);
        $this->assertResponseStatus(200);
    }

    public function testGetUserFails()
    {
        $this->call('GET', 'api/user/fake/user');
        $this->assertResponseStatus(404);
    }

    public function testGetEmail()
    {
        $this->call('GET', 'api/user/' . 'test@mail.nl');
        $this->assertResponseStatus(200);
    }

    public function testGetEmailFails()
    {
        $user = $this->createValidUser();
        $user->save();

        $this->call('GET', 'api/user/' . 'test@mail.nl');
        $this->assertResponseStatus(400);
    }

    public function testGetLinks()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $link = $this->createValidLink($creator);
        $link->save();

        $question = $this->createValidQuestion($creator);
        $question->save();

        $user = $this->createValidUser();
        $user->save();

        $link->question()->attach($question->id);

        $this->call('GET', 'api/link/' . $user->id);        
        $this->assertResponseStatus(200);
    }

    public function testGetLinksFails()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $link = $this->createValidLink($creator);
        $link->save();

        $user = $this->createValidUser();
        $user->save();

        $this->call('GET', 'api/link/' . $user->id);        
        $this->assertResponseStatus(404);
    }  

    public function testStoreAnswer()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $link = $this->createValidLink($creator);
        $link->save();

        $question = $this->createValidQuestion($creator);
        $question->save();

        $user = $this->createValidUser();
        $user->save();

        $input = array(
            'user_id' => $user->id,
            'link_id' => $link->id,
            'question_id' => $question->id,
            'answer' => 'Testantwoord',
            'responsetime' => 1234
        );

        $this->call('POST', 'api/answer', $input);

        $this->assertResponseStatus(200);
        $answer = Answer::first();
        $this->assertTrue($answer->user_id == $user->id);
        $this->assertTrue($answer->link_id == $link->id);
        $this->assertTrue($answer->question_id == $question->id);
        $this->assertTrue($answer->answer == 'Testantwoord');
        $this->assertTrue($answer->responsetime == 1234);

        $user = User::findOrFail($user->id);
        $this->assertTrue($user->points == 1);
    }

    public function testStoreAnswerFails()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $link = $this->createValidLink($creator);
        $link->save();

        $question = $this->createValidQuestion($creator);
        $question->save();

        $user = $this->createValidUser();
        $user->save();

        $input = array(
            'answer' => 'Invalid answer'
        );

        $this->call('POST', 'api/answer', $input);
        $this->assertResponseStatus(400);

        $this->assertTrue(Answer::all()->isEmpty());
    }

     protected function createValidHogeschool(){
        $hogeschool = new Hogeschool;
        $hogeschool->id = 1;
        $hogeschool->hogeschool = 'test';
        return $hogeschool;
    }

    protected function createValidCreator(){
        $creator = new Creator;
        $creator->id = 2;
        $creator->evaluatie_hogeschool_id = 1;
        $creator->name = 'test';
        $creator->email = 'test@test.nl';
        $creator->password = "Test1234";
        return $creator;
    }

    protected function createValidQuestion($creator){
        $question = new Question;
        $question->id = 1;
        $question->evaluatie_creator_id = $creator->id;
        $question->type = 'janee';
        $question->question = 'Dit is een testvraag';
        return $question;
    }

    protected function createValidLink($creator){
        $currentDate = Carbon\Carbon::now();
        $week = $currentDate->format('W');
        $year = $currentDate->format('o');

        $link = new Link;
        $link->id = 1;
        $link->evaluatie_creator_id = $creator->id;
        $link->week = $week;
        $link->year = $year;
        return $link;
    }

    protected function createValidUser(){
        $user = new User;
        $user->id = 1;
        $user->unikey = 'test';
        $user->email = 'test@mail.nl';
        $user->password = 'Test1234';
        return $user;
    }
}