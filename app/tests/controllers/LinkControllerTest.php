<?php

class LinkControllerTest extends TestCase {

    protected $useDatabase = true;

	public function testIndex()
	{
		$response = $this->call('GET', 'link');
		
        $view = $response->getOriginalContent();
        $this->assertTrue($view instanceof Illuminate\View\View);
        $this->assertViewHas('links');
    }

    public function testStore()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $input = array(
            'Link' => array(
                'week' => 20,
                'year' => 2015
            )
        );

        $response = $this->call('POST', 'link', $input);

        $link = DB::table('evaluatie_link')->orderBy('id', 'desc')->first();

        $view = $response->getOriginalContent();
        $this->assertTrue($view instanceof Illuminate\View\View);
        $this->assertViewHas('link');
        $this->assertViewHas('answers');
        $this->assertTrue($link->week == 20);
        $this->assertTrue($link->year == 2015);
    }

    public function testStoreWithInvalidInput()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $input = array(
            'Link' => array(
                'week' => -33,
                'year' => 1
            )
        );

        $response = $this->call('POST', 'link', $input, array(), array('HTTP_REFERER' => route('link.index')));

        $this->assertRedirectedToAction('LinkController@index');
        $this->assertTrue($response->getSession()->has('errors'));
        $this->assertTrue($response->getSession()->get('errors')->count() == 2);
    }

    public function testShow()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $link = $this->createValidLink($creator);
        $link->save();

        $response = $this->call('GET', 'link/1');
        
        $view = $response->getOriginalContent();
        $this->assertTrue($view instanceof Illuminate\View\View);
        $this->assertViewHas('link');
    }

    public function testAttach()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $question = $this->createValidQuestion($creator);
        $question->save();

        $link = $this->createValidLink($creator);
        $link->save();

        $response = $this->call('POST', 'link/' . $link->id . '/' . $question->id);
        $questions = $link->question;

        $this->assertRedirectedToAction('LinkController@show', array('id' => $link->id));
        $this->assertTrue(count($questions) == 1);
    }

    protected function createValidHogeschool(){
        $hogeschool = new Hogeschool;
        $hogeschool->id = 1;
        $hogeschool->hogeschool = 'test';
        return $hogeschool;
    }

    protected function createValidCreator(){
        $creator = new Creator;
        $creator->id = 2;
        $creator->evaluatie_hogeschool_id = 1;
        $creator->name = 'test';
        $creator->email = 'test@test.nl';
        $creator->password = "Test1234";
        return $creator;
    }

    protected function createValidQuestion($creator){
        $question = new Question;
        $question->id = 1;
        $question->evaluatie_creator_id = $creator->id;
        $question->type = 'janee';
        $question->question = 'Dit is een testvraag';
        return $question;
    }

    protected function createValidLink($creator){
        $link = new Link;
        $link->id = 1;
        $link->evaluatie_creator_id = $creator->id;
        $link->week = 10;
        $link->year = 2015;
        return $link;
    }
}
