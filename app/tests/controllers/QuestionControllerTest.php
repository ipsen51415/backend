<?php

class QuestionControllerTest extends TestCase {

    protected $useDatabase = true;

	public function testIndex()
	{
		$response = $this->call('GET', 'question');
		
        $view = $response->getOriginalContent();
        $this->assertTrue($view instanceof Illuminate\View\View);
        $this->assertViewHas('questions');
    }

    public function testStore()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $input = array(
            'Question' => array(
                'type' => 'janee',
                'question' => 'Dit is een testvraag'
            )
        );

        $response = $this->call('POST', 'question', $input);

        $question = DB::table('evaluatie_question')->orderBy('id', 'desc')->first();

        $view = $response->getOriginalContent();
        $this->assertTrue($view instanceof Illuminate\View\View);
        $this->assertViewHas('question');
        $this->assertTrue($question->type == 'janee');
        $this->assertTrue($question->question == 'Dit is een testvraag');
    }

    public function testStoreWithInvalidInput()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $input = array(
            'Question' => array(
                'type' => 'janee'
            )
        );

        $response = $this->call('POST', 'question', $input, array(), array('HTTP_REFERER' => route('question.index')));

        $this->assertRedirectedToAction('QuestionController@index');
        $this->assertTrue($response->getSession()->has('errors'));
        $this->assertTrue($response->getSession()->get('errors')->count() == 1);
    }

    public function testShow()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $question = $this->createValidQuestion($creator);
        $question->save();

        $response = $this->call('GET', 'question/1');
        
        $view = $response->getOriginalContent();
        $this->assertTrue($view instanceof Illuminate\View\View);
        $this->assertViewHas('question');
    }

    public function testUpdate()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $question = $this->createValidQuestion($creator);
        $question->save();

        $input = array(
            'Question' => array(
                'type' => 'ster',
                'question' => 'aangepaste vraag'
            )
        );

        $this->assertTrue(Question::find(1)->type == 'janee');
        $this->assertTrue(Question::find(1)->question == 'Dit is een testvraag');

        $response = $this->call('PUT', 'question/1', $input);

        $view = $response->getOriginalContent();
        $this->assertTrue($view instanceof Illuminate\View\View);
        $this->assertViewHas('question');
        $this->assertTrue(Question::find(1)->type == 'ster');
        $this->assertTrue(Question::find(1)->question == 'aangepaste vraag');
    }

    public function testDestroy()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $question = $this->createValidQuestion($creator);
        $question->save();

        $this->assertTrue(count(Question::all()) == 1);
        $response = $this->call('DELETE', 'question/1');

        $this->assertRedirectedToAction('QuestionController@index');
        $this->assertTrue(count(Question::all()) == 0);

    }

    public function testAttach()
    {
        $hogeschool = $this->createValidHogeschool();
        $hogeschool->save();

        $creator = $this->createValidCreator();
        $creator->save();

        $link = $this->createValidLink($creator);
        $link->save();

        $question = $this->createValidQuestion($creator);
        $question->save();

        $response = $this->call('POST', 'question/1/1');
        $links = $question->link;

        $this->assertRedirectedToAction('QuestionController@show', array('id' => $question->id));
        $this->assertTrue(count($links) == 1);
    }

    protected function createValidHogeschool(){
        $hogeschool = new Hogeschool;
        $hogeschool->id = 1;
        $hogeschool->hogeschool = 'test';
        return $hogeschool;
    }

    protected function createValidCreator(){
        $creator = new Creator;
        $creator->id = 2;
        $creator->evaluatie_hogeschool_id = 1;
        $creator->name = 'test';
        $creator->email = 'test@test.nl';
        $creator->password = "Test1234";
        return $creator;
    }

    protected function createValidQuestion($creator){
        $question = new Question;
        $question->id = 1;
        $question->evaluatie_creator_id = $creator->id;
        $question->type = 'janee';
        $question->question = 'Dit is een testvraag';
        return $question;
    }

    protected function createValidLink($creator){
        $link = new Link;
        $link->id = 1;
        $link->evaluatie_creator_id = $creator->id;
        $link->week = 10;
        $link->year = 2015;
        return $link;
    }
}
