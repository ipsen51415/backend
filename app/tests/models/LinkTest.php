<?php
	
	class LinkTest extends TestCase{
		
		public function testCreate(){
            $hogeschool = $this->createValidHogeschool();
            $hogeschool->save();
            $creator = $this->createValidCreator($hogeschool);
            $creator->save();
            $link = $this->createValidLink($creator);
            $link->save();

            $this->assertTrue($link->exists);
            $this->assertTrue(Link::all()->count() == 1);
            $this->assertEquals($creator->id, Link::first()->evaluatie_creator_id);
            $this->assertEquals(10, Link::first()->first()->week);
            $this->assertEquals(2015, Link::first()->year);

		}
		
		protected function createValidHogeschool(){
        	$hogeschool = new Hogeschool;
        	$hogeschool->id = 2;
        	$hogeschool->hogeschool = 'test2';
        	return $hogeschool;
    	}

	    protected function createValidCreator(){
    	    $creator = new Creator;
    	    $creator->id = 2;
    	    $creator->evaluatie_hogeschool_id = 2;
    	    $creator->name = 'test';
    	    $creator->email = 'tester@test.nl';
    	    $creator->password = "Test1234";
        	return $creator;
    	}

    	protected function createValidLink($creator){
        	$link = new Link;
        	$link->id = 1;
       		$link->evaluatie_creator_id = $creator->id;
        	$link->week = 10;
        	$link->year = 2015;
        	return $link;
    	}
	}

?>