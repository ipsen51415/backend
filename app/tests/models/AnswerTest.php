<?php
	
class AnswerTest extends TestCase{
	
	public function testCreate(){
		
		$hogeschool = $this->createValidHogeschool();
		$hogeschool->save();
		$creator = Auth::user();
		$question = $this->createValidQuestion($creator);
		$question->save();
		$link = $this->createValidLink($creator);
		$link->save();
		$link->question()->attach($question->id);
		$user = $this->createValidUser();
		$user->save();
		$answer = $this->createValidAnswer($user, $link, $question);
		$answer->save();
		
		$this->assertTrue($answer->exists);
		$this->assertTrue(Answer::all()->count() == 1);
		$this->assertEquals($user->id, Answer::first()->user_id);
		$this->assertEquals($question->id, Answer::first()->question_id);
		$this->assertEquals('test', Answer::first()->answer);
		$this->assertEquals(1234, Answer::first()->responsetime);
	}
	public function testGetAuthIdentifier(){
		$hogeschool = $this->createValidHogeschool();
		$hogeschool->save();
		$creator = Auth::user();
		$question = $this->createValidQuestion($creator);
		$question->save();
		$link = $this->createValidLink($creator);
		$link->save();
		$link->question()->attach($question->id);
		$user = $this->createValidUser();
		$user->save();
		$answer = $this->createValidAnswer($user, $link, $question);
		$answer->save();
		
		$this->assertEquals(1, $answer->getAuthIdentifier());
	}
	public function testGetAnswer(){
		$hogeschool = $this->createValidHogeschool();
		$hogeschool->save();
		$creator = Auth::user();
		$question = $this->createValidQuestion($creator);
		$question->save();
		$link = $this->createValidLink($creator);
		$link->save();
		$link->question()->attach($question->id);
		$user = $this->createValidUser();
		$user->save();
		$answer = $this->createValidAnswer($user, $link, $question);
		$answer->save();
		
		$this->assertEquals('test', $answer->getAuthAnswer());
	}
	public function testGetAuthResponsetime(){
		$hogeschool = $this->createValidHogeschool();
		$hogeschool->save();
		$creator = Auth::user();
		$question = $this->createValidQuestion($creator);
		$question->save();
		$link = $this->createValidLink($creator);
		$link->save();
		$link->question()->attach($question->id);
		$user = $this->createValidUser();
		$user->save();
		$answer = $this->createValidAnswer($user, $link, $question);
		$answer->save();
		
		$this->assertEquals(1234, $answer->getAuthResponsetime());
	}
	public function testTableAtrribute(){
		$answer = new Answer();
		$this->assertEquals('evaluatie_answer', $answer->getTable());
	}

	protected function createValidHogeschool(){
    	$hogeschool = new Hogeschool;
    	$hogeschool->id = 2;
    	$hogeschool->hogeschool = 'test2';
    	return $hogeschool;
	}

	protected function createValidQuestion($creator){
    	$question = new Question;
    	$question->id = 1;
    	$question->evaluatie_creator_id = Auth::user()->id;
    	$question->type = 'janee';
    	$question->question = 'Dit is een testvraag';
    	return $question;
	}

	protected function createValidLink($creator){
    	$link = new Link;
    	$link->id = 1;
   		$link->evaluatie_creator_id = Auth::user()->id;
    	$link->week = 10;
    	$link->year = 2015;
    	return $link;
	}
	protected function createValidUser(){
    	$user = new User;
    	$user->id = 1;
    	$user->unikey = 'test';
    	$user->email = 'test@test.nl';
    	$user->password = 'test';
    	return $user;
	}
	protected function createValidAnswer($user, $link, $question){
	
		$answer = new Answer;
		$answer->id = 1;
		$answer->user_id = $user->id;
		$answer->link_id = $link->id;
		$answer->question_id = $question->id;
		$answer->answer = 'test';
		$answer->responsetime = 1234;
		return $answer;
	}
	
}
?>