<?php
	
	class QuestionTest extends TestCase{
		
		public function testCreate(){
            $hogeschool = $this->createValidHogeschool();
            $hogeschool->save();
            $creator = $this->createValidCreator($hogeschool);
            $creator->save();

            $question = $this->createValidQuestion($creator);
            $question->save();

            $this->assertTrue($question->exists);
            $this->assertTrue(Question::all()->count() == 1);
            $this->assertEquals($question->evaluatie_creator_id, Question::first()->id);
            $this->assertEquals('janee', Question::first()->type);
            $this->assertEquals('Dit is een testvraag', Question::first()->question);
		}
		
		protected function createValidHogeschool(){
        	$hogeschool = new Hogeschool;
        	$hogeschool->id = 2;
        	$hogeschool->hogeschool = 'test';
        	return $hogeschool;
    	}

	    protected function createValidCreator($hogeschool){
    	    $creator = new Creator;
    	    $creator->id = 2;
    	    $creator->evaluatie_hogeschool_id = 2;
    	    $creator->name = 'test';
    	    $creator->email = 'tester@test.nl';
    	    $creator->password = "Test1234";
        	return $creator;
    	}
        protected function createValidQuestion($creator){
        $question = new Question;
        $question->id = 1;
        $question->evaluatie_creator_id = Auth::user()->id;
        $question->type = 'janee';
        $question->question = 'Dit is een testvraag';
        return $question;
    }
	}

?>