<?php
	
	class HogeschoolTest extends TestCase{
		
		public function testCreate(){
			$hogeschool = $this->createValidHogeschool();
            $hogeschool->save();

            $this->assertTrue($hogeschool->exists);
            $this->assertTrue(Hogeschool::all()->count() == 2);
            $this->assertEquals($hogeschool->id, Hogeschool::orderBy('id', 'desc')->first()->id);
		}
		
		protected function createValidHogeschool(){
        	$hogeschool = new Hogeschool;
        	$hogeschool->id = 2;
        	$hogeschool->hogeschool = 'test2';
        	return $hogeschool;
    	}
	}
?>