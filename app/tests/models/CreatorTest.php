<?php
	
	class CreatorTest extends TestCase{
		
		public function testCreate(){
			$hogeschool = $this->createValidHogeschool();
            $hogeschool->save();

            $creator = $this->createValidCreator($hogeschool);
            $creator->save();

            $this->assertTrue($creator->exists);
            $this->assertTrue(Creator::all()->count() == 2);
            $this->assertEquals($hogeschool->id, Creator::orderBy('id', 'desc')->first()->evaluatie_hogeschool_id);
            $this->assertEquals('test', Creator::orderBy('id', 'desc')->first()->name);
            $this->assertEquals('tester@test.nl', Creator::orderBy('id', 'desc')->first()->email);
            $this->assertEquals('test', Creator::orderBy('id', 'desc')->first()->password);
		}
		
		protected function createValidHogeschool(){
        	$hogeschool = new Hogeschool;
        	$hogeschool->id = 2;
        	$hogeschool->hogeschool = 'test2';
        	return $hogeschool;
    	}
        protected function createValidCreator($hogeschool){
    
            $creator = new Creator;
            $creator->id = 2;
            $creator->name = 'test';
            $creator->email = 'tester@test.nl';
            $creator->password = 'test';
            $creator->evaluatie_hogeschool_id = $hogeschool->id;
            return $creator;
        }
	}
?>