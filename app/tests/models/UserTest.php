<?php
	
	class UserTest extends TestCase{
		
		public function testCreate(){
            $user = $this->createValidUser();
            $user->save();

            $this->assertTrue($user->exists);
            $this->assertTrue(User::all()->count() == 1);
            $this->assertEquals(1, User::first()->id);
            $this->assertEquals('test', User::first()->unikey);
            $this->assertEquals('test@test.nl', User::first()->email);
            $this->assertEquals('test', User::first()->password);
		}
		
		protected function createValidUser(){
            $user = new User;
            $user->id = 1;
            $user->unikey = 'test';
            $user->email = 'test@test.nl';
            $user->password = 'test';
            return $user;
    }
	}

?>