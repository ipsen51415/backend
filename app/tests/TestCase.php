<?php

use Mockery as m;

class TestCase extends Illuminate\Foundation\Testing\TestCase {

    protected $useDatabase = true;

    /**
     * Creates the application.
     *
     * @return Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        $unitTesting = true;

        $testEnvironment = 'testing';

        return require __DIR__.'/../../bootstrap/start.php';
    }

    public function setUp()
    {
        parent::setUp();
        if($this->useDatabase)
        {
            $this->setUpDb();
        }
        $this->authenticate();
    }

    public function teardown()
    {
        m::close();
    }

    public function setUpDb()
    {
        Artisan::call('migrate');
        Artisan::call('db:seed');
    }

    public function teardownDb()
    {
        Artisan::call('migrate:reset');
    }

    public function authenticate()
    {
        $hogeschool = new Hogeschool;
        $hogeschool->id = 1;
        $hogeschool->hogeschool = 'test';
        $hogeschool->save();

        $creator = new Creator;
        $creator->id = 1;
        $creator->evaluatie_hogeschool_id = 1;
        $creator->name = 'test';
        $creator->email = 'test@test.nl';
        $creator->password = "Test1234";
        $creator->save();

        $this->be($creator);
    }
}