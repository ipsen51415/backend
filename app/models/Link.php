<?php

use LaravelBook\Ardent\Ardent;

class Link extends Ardent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'evaluatie_link';

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

	/**
	 * The attributes that are not mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array(
		'id'
	);

	/**
	 * Get the unique identifier for the link.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the week for the link.
	 *
	 * @return string
	 */
	public function getAuthWeek()
	{
	  return $this->week;
	}

	/**
	 * Get the year for the link.
	 *
	 * @return string
	 */
	public function getAuthYear()
	{
	  return $this->year;
	}

	/**
	 * Ardent validation rules
	 */
	public static $rules = array(
		'week' => array('required', 'numeric', 'regex:/^(([1-4][0-9])|(0[1-9])|(5[0-2]))$/'),
		'year' => 'required|min:4|max:4',
	);

	/**
	* Ardent error messages
	*/
	public static $customMessages = array(
	    'required' => 'Er moet een week en jaar worden ingevoerd.',
	    'between' => 'Voor week moet een waarde ingevoerd worden tussen 1 en 52.',
	    'numeric' => 'Voor week kunnen alleen nummers ingevoerd worden.',
	    'regex' => 'Het formaat van week is ongeldig.',
	    'min' => 'De ingevoerde waarde van jaar is te kort',
	    'max' => 'De ingevoerde waarde van jaar is te lang',
  	);

	// Relations
	public function creator(){ 
		return $this->belongsTo('Creator', 'evaluatie_creator_id', 'id');
	}
	public function question(){
		return $this->belongsToMany('Question');
	}
	public function user(){
		return $this->belongsToMany('User');
	}
	public function availableQuestions()
	{
	    $ids = \DB::table('link_question')->where('link_id', '=', $this->id)->lists('question_id');
	    return \Question::whereNotIn('id', $ids)->get();
	}
}
?>