<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use LaravelBook\Ardent\Ardent;

class User extends Ardent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = array(
		'points',
	);

	/**
	 * The attributes with default values.
	 *
	 * @var array
	 */
	protected $attributes = array(
  		'points' => 0,
	);

	/**
	 * The attributes that are not mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array(
		'id',
		'unikey',
		'email',
		'password',
	);

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the unikey for the user.
	 *
	 * @return string
	 */
	public function getAuthUnikey()
	{
	  return $this->unikey;
	}

	/**
	 * Get the email for the user.
	 *
	 * @return string
	 */
	public function getAuthEmail()
	{
	  return $this->email;
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
	  return $this->password;
	}

	/**
	 * Get the points for the user.
	 *
	 * @return string
	 */
	public function getAuthPoints()
	{
	  return $this->points;
	}

	/**
	 * Ardent validation rules
	 */
	public static $rules = array(
		'unikey' => 'required',
		'email' => 'required|email',
		'password' => 'required',
	);

	// Relations
	public function answer(){
		return $this->hasMany('Answer', 'user_id');
	}
	public function link(){
		return $this->belongsToMany('Link');
	}
}
