<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use LaravelBook\Ardent\Ardent;

class Creator extends Ardent implements UserInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'evaluatie_creator';

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * The attributes that are not mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array(
		'id',
		'name',
		'email',
		'password',
	);
    
    /**
    * Abstract methods that are inherited from the UserInterface.
    * Since we don't support a remember me (while logging in) function
    * we leave these methods as is.
    */
    public function getRememberToken()
    {
    }
    
    public function setRememberToken($value)
    {
    }
    
    public function getRememberTokenName()
    {
    }

	/**
	 * Get the unique identifier for the creator.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the name for the creator.
	 *
	 * @return string
	 */
	public function getAuthName()
	{
	  return $this->name;
	}

	/**
	 * Get the email for the creator.
	 *
	 * @return string
	 */
	public function getAuthEmail()
	{
	  return $this->email;
	}

	/**
	 * Get the password for the creator.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
	  return $this->password;
	}

	/**
	 * Ardent validation rules
	 */
	public static $rules = array(
		'name' => 'required',
		'email' => 'required|email',
		'password' => 'required',
	);

	// Relations
	public function hogeschool(){ 
		return $this->belongsTo('Hogeschool', 'evaluatie_hogeschool_id', 'id');
	}
	public function link(){
		return $this->hasMany('Link', 'evaluatie_creator_id');
	}
	public function question(){
		return $this->hasMany('Question', 'evaluatie_creator_id');
	}

}
?>