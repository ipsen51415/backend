<?php

use LaravelBook\Ardent\Ardent;

class Answer extends Ardent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'evaluatie_answer';

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

	/**
	 * The attributes that are not mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array(
		'id',
		'answer',
		'responsetime',
	);

	/**
	 * Get the unique identifier for the answer.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the answer for the answer.
	 *
	 * @return string
	 */
	public function getAuthAnswer()
	{
	  return $this->answer;
	}

	/**
	 * Get the responsetime for the answer.
	 *
	 * @return string
	 */
	public function getAuthResponsetime()
	{
	  return $this->responsetime;
	}

	/**
	 * Ardent validation rules
	 */
	public static $rules = array(
		'answer' => 'required',
		'responsetime' => 'required',
	);

	// Relations
	public function user(){
		return $this->belongsTo('User', 'user_id', 'id');
	}
	public function question(){
		return $this->belongsTo('Question', 'evaluatie_question_id', 'id');
	}
}
?>