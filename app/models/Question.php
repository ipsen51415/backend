<?php

use LaravelBook\Ardent\Ardent;

class Question extends Ardent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'evaluatie_question';


	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = array(
		'type',
		'question',
	);

	/**
	 * The attributes that are not mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array();

	/**
	 * Get the unique identifier for the question.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the type for the question.
	 *
	 * @return string
	 */
	public function getAuthType()
	{
	  return $this->type;
	}

	/**
	 * Get the type for the question.
	 *
	 * @return string
	 */
	public function getAuthQuestion()
	{
	  return $this->question;
	}


	/**
	 * Ardent validation rules
	 */
	public static $rules = array(
		'type' => 'required',
		'question' => 'required',
	);

	/**
	* Ardent error messages
	*/
	public static $customMessages = array(
    'required' => 'Er moet een vraag worden ingevoerd.',
  	);

	// Relations
	public function creator(){ 
		return $this->belongsTo('Creator', 'evaluatie_creator_id', 'id');
	}
	public function link(){
		return $this->belongsToMany('Link');
	}
	public function answer(){
		return $this->hasMany('Answer', 'evaluatie_question_id');
	}
	public function availableLinks()
	{
	    $ids = \DB::table('link_question')->where('question_id', '=', $this->id)->lists('link_id');
	    return \Link::whereNotIn('id', $ids)->get();
	}
}
?>