<?php

use LaravelBook\Ardent\Ardent;

class Hogeschool extends Ardent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'evaluatie_hogeschool';

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * The attributes that are not mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array(
		'id',
		'hogeschool',
	);

	/**
	 * Get the unique identifier for the hogeschool.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the hogeschool for the hogeschool.
	 *
	 * @return string
	 */
	public function getAuthHogeschool()
	{
	  return $this->hogeschool;
	}


	/**
	 * Ardent validation rules
	 */
	public static $rules = array(
		'hogeschool' => 'required|unique:evaluatie_hogeschool',
	);

	// Relations
	public function creator(){
		return $this->hasMany('Creator', 'evaluatie_hogeschool_id');
	}
}
?>